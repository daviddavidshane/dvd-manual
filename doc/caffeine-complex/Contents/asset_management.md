Asset Management
================
**Written by David Nuon**

## Motivation

We wrote a lot of code for this game, approximately 400+ commits over 14.7k lines worth of code. So in order to make sure things were running as smoothly as possible, I took it upon myself to make sure we had the adequate tools to manage this project.

## Source Control

Git was the first choice I had in mind when thinking about which source control system to use. It was easy to use and allowed us to make commits and track progress without the need to be connected to the internet. This was essential to the success of the projects since many of us had to make changes while offline. It also allowed us to do one more thing and that was to do major changes on the code base before comitting it. That way the others on the team would only see the good project of the changes instead of the steps inbetween that could break the team's codebase. 

We thought about using something like Dropbox however, it was soon decided that it would not be a good idea since when a team member made a change in the codebase, it would immeadiately be reflected for the others. This meant that, say, if I were to accidently delete everything in the directory, Shane and David would have their code deleted also.

But git does much more than simply keeping track of our code. Git logs everything and we can use that to generate statistics. This was used as a motivator for us, as this was a large project to take on in a semester's time. 

## Documentation

### Engine 
Shane was making an engine so we needed a way to keep track of what was going on. We attempted to draw models with tools like Dia or Visio and use well annotated code that allowed easy navigation. However, this became unwieldly as the codebase reached the 5k line mark. No longer was it possible to keep on doin this by hand and being able to actually work on the game. There is were I used doxygen to document everything. 

We still kept high level models on paper, however, we annotated our code and used doxygen to generate our API documentation. Some might see this as a lazy way to do things, however, for us it was the only way. The on feature that was really handy for us were call graphs and high level object relationships that were generated. This allowed us to see what was going on in a visual, high level and allowed us to debug much easier.

### Other
We used easybook, a book generator that uses PHP and PrinceXML to generate other doucments. It takes in markdown and transforms it into a website or PDF document with little effort. Since it was all text-based it is less likely to be corrutped like use a Word document and we could easily keep track of it in git. 

I also devised a way for the markdown documents to have python code arbitrarily execute inside them. This allowed really complicated logic that was easy to implement that didn't require to have twig template code involved.

## Non-code Assets

Since I was the only one handling the art and music, I used Dropbox to manage this. Git cannot handle binary files very well, and since I wasn't working with anyone on these aspects, it made sense for me to do this. There are about 1.04 GB worth of assets included on the disc.