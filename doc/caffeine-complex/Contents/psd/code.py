import os
from subprocess import *

GIT_COMMIT_FIELDS = ['id', 'author_name', 'author_email', 'date', 'message']
GIT_LOG_FORMAT = ['%H', '%an', '%ae', '%ad', '%s']

GIT_LOG_FORMAT = '%x1f'.join(GIT_LOG_FORMAT) + '%x1e'

p = Popen('git log --format="%s"' % GIT_LOG_FORMAT, shell=True, stdout=PIPE)
(log, _) = p.communicate()
log = log.strip('\n\x1e').split("\x1e")
log = [row.strip().split("\x1f") for row in log]
log = [dict(zip(GIT_COMMIT_FIELDS, row)) for row in log]
log = [l for l in log if l['message'].startswith('release:')]

log = [[l['message'].replace('release:', '').strip(), l['date'], '2 Davids and a Shane'] for l in log][::-1]


log = map(lambda x: tuple([x[0]] + x[1]), zip(range(1, len(log) + 1), log))[::-1]

format = """
<tr>
	<td>%s</td>
	<td>%s</td>
	<td>%s</td>
	<td>%s</td>
</tr>
"""

head = """
<thead>
	<td>%s</td>
	<td>%s</td>
	<td>%s</td>
	<td>%s</td>
</thead>
"""

print "<table>"
print head % ('Rev', 'Description', 'Date', 'Author(s)')
print "<tbody>"
for n in log:
	print format % n
	print
	
print "</tbody></table>"