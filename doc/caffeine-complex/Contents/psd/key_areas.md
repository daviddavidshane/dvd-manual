### Key Areas

#### Performance

(Martel and Shane need to answer this)

#### Security

Anyone will be able to obtain the program, no authentication is needed.  The user needs to be able to execute binaries on their system. 

#### Documentation

The documentation included will consist of an in-game help and reference section along with developer notes. 

Engine API and build instructions will also be available. All content can be found in our BitBucket repository.

#### Infastructrue

The binary distributed will run on any Windows 7 or Linux system.

To build the software, certain dependencies must be installed:
	
	- SDL
	- OpenGL
	- GLEW
	- DevIL


#### Hardware

The user needs a mouse in order to play the game along with any system running Windows 7 or Linux.

Recommended System Requirements:
	- Windows 7 or Ubuntu 12.04 LTS
	- 2.2+ GHz single-core processor (dual-core recommended)
	- 2 GB RAM 
	- OpenGL 2.1 Compatible Graphics Card
	- Use these or better for good performance:
	- GeForce 8600 GS
	- Radeon HD 3470
	- 150 MB for game files
	- Widescreen (16:9) resolution display is recommended
	- At least 1280px * 720px
	- Keyboard
	- 2+ Button Mouse
	- Speakers


#### Licensed Software

The game distribution does not require any other software products to be installed to run.

