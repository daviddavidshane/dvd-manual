Use Cases
=========

	*Use Case:* Modifying Game
		*Primary Actor:* User
		*Goal in Context:* Modify game to users preference
		*Preconditions:* User has downloaded source and all dependencies.
		*Trigger:* User wants to change game
		*Scenario:*
			1. User wants to change game
			2. User edits source code
			3. User compiles code
			4. User tests changes
		Exceptions
			1. User does not know how to code
			2. User creates a bug and cannot compile
		*Priority:* Essential
		*Frequency:* Almost every running of game


	*Use Case:* Playing Game
		*Primary Actor:* User
		*Goal in Context:* Playing the game
		*Preconditions:* User has downloaded source and all dependencies.
		*Trigger:* User wants to use the software
		*Scenario:*
			1. User runs game 
			2. User plays game
			3. User exits
		Exceptions
			1. User has incompatible machine
			2. User creates a bug and cannot compile
		*Priority:* Essential
		*Frequency:* Almost every running of game


	*Use Case:* Spawing Units
		*Primary Actor:* User
		*Goal in Context:* add units to game
		*Preconditions:* player has enough money, there is room for units
		*Trigger:* Player decides to make units
		*Scenario:*
			1. User moves mouse to where units will spawn
			2. User hits space
			3. Units spawn
		Exceptions
			1. Not enough money to spawn units
			2. Not enough room for units
			3. User has hit unit cap
		*Priority:* Essential for gameplay
		*Frequency:* Constantly during game

	*Use Case:* Kill Enemies
		*Primary Actor:* User
		*Goal in Context:* Assist units in killing enemies
		*Preconditions:* player has enough money
		*Trigger:* Player wants units to be more effective at killing
		*Scenario:*
			1. Units are not advancing fast enough.
			2. Player presses key for magic.
			3. Units near mouse receive buff or damage
		*Priority:* Essential for balanced gameplay
		*Frequency:* Constantly during game
		*Use Case:* Have Fun
		*Primary Actor:* User
		*Goal in Context:* User wants enjoyment
		*Preconditions:* None
		*Trigger:* User wants to have fun
		*Scenario:*
			1. User starts game
			2. User beats game
			3. Enjoyment
		*Priority:* Essential
		*Frequency:* Constantly
