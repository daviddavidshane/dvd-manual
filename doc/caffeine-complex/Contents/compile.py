import os
import sys
import StringIO

def get_content(s):
    out = ""
    with open(s) as f:
        out = f.read()

    return out

def execute_code(c):
    c = get_content(c)
    out = ""

    # create file-like string to capture output
    codeOut = StringIO.StringIO()
    codeErr = StringIO.StringIO()

    # capture output and errors
    sys.stdout = codeOut
    sys.stderr = codeErr

    exec c
    out = codeOut.getvalue()
    # restore stdout and stderr
    sys.stdout = sys.__stdout__
    sys.stderr = sys.__stderr__

    codeOut.close()
    codeErr.close()####

    return out


def resolve(l):
    out = []
    for k in l:
        if k.startswith('::'):
            if k.endswith('.py'):
                out.append(execute_code(k[2:]))     
            elif k.endswith('.md'):
                out.append(get_content(k[2:]))
            else:
                out.append(k)
        else:
            out.append(k)
    return out

def make(i,o):
    with open(i) as f:
        compy = resolve(f.read().split('\n'))

    with open(o,"w") as f:
        f.write("\n".join(compy))

if __name__ == '__main__':  
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    make("psd.base", "psd.md.compiled")
    make("manual.base", "manual.md.compiled")
    make("lore.base", "lore.md.compiled")
    make("software.base", "software.md.compiled")
    make("code.base", "code.md.compiled")
    compy = []
