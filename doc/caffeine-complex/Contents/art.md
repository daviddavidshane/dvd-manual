Art and Theme
=============
![Cover Page + Motif](415%20Cover%20Page%20+%20Motif.jpg)

## Concept Art 

![The icon that inspired the theme](445%20This%20icon%20inspired%20much%20of%20the%20game_%20It's%20also%20very%20creepy.jpg)
Orginally the idea for Caffine Complex was originally "dark and edgy" for the sake of it. It was inspired by this image.

As time went on, we developed more and more ideas, some of it stayed in the game. The game was originally intended to be able corrupt churches, vikings and evil duct tape, but we felt the the theme would to too difficult to explain or it felt too contrived.

![Game was originally going to have a style similar to this, was scrapped because it was too labor intensive](398%20Game%20was%20originally%20going%20to%20have%20a%20style%20similar%20to%20this,%20was%20scrapped%20because%20it%20was%20too%20labor%20int.jpg)
![Before caffine, the game was about chains and bondage](402%20Before%20Caffeine,%20we%20had%20chains.jpg)
![Mockup of original gameplay concept](405%20Battle%20Mockup.jpg)


![Orginal design for Shamus](406%20Shamus.jpg)
![The original Concept for Shamus](442%20The%20original%20Concept%20for%20Shamus.jpg)

![Jack colored in](419%20Jack%20colored%20in.jpg)
![Jack 0](407%20Jack%200.jpg)
![Jack 1](408%20Jack%201.jpg)
![Jack 2](409%20Jack%202.jpg)
![Jack 3](410%20Jack%203.jpg)
![Jack 4](411%20Jack%204.jpg)
![Jack 5](413%20Jack%205.jpg)
![An early Jack_ The game was meant to be dark and edgy instead of colorful and jazzy as it is now](426%20An%20early%20Jack_%20The%20game%20was%20meant%20to%20be%20dark%20and%20edgy%20instead%20of%20colorful%20and%20jazzy%20as%20it%20is%20now.jpg)
![Original Concept for Jolie and Gloria](412%20Original%20Concept%20for%20Jolie%20and%20Gloria.jpg)
![For a certain time, the game was about duct tape](416%20For%20a%20certain%20time,%20the%20game%20was%20about%20duct%20tape.jpg)
![The game was originally called _Dudes vs Dudes_ because we couldn't find a better name](417%20The%20game%20was%20originally%20called%20_Dudes%20vs%20Dudes_%20because%20we%20couldn't%20find%20a%20better%20name.jpg)
![Earl Karl the Jarl - The idea was an accountant that had viking armor that materialized on, ideas of this was eventually incorporated in the final Karl](422%20Earl%20Karl%20the%20Jarl%20-%20The%20idea%20was%20an%20accountant%20that%20had%20viking%20armor%20that%20materialized%20on,%20ideas%20of.jpg)
![Robert was originally meant to be a Father of a corrupt church](434%20Robert%20was%20originally%20meant%20to%20be%20a%20Father%20of%20a%20corrupt%20church.jpg)
![The first boss of the game, scrapped when duct tape was no longer a theme](435%20The%20first%20boss%20of%20the%20game,%20scrapped%20when%20duct%20tape%20was%20no%20longer%20a%20theme.jpg)
![This happened, apparently](440%20This%20happened,%20apparently.jpg)

![There was a time we played around with the game taking place in Europe](449%20There%20was%20a%20time%20we%20played%20around%20with%20the%20game%20taking%20place%20in%20Europe.jpg)
![Karl x 1960](446%20Karl%20x%201960.jpg)
![Shamus x 1980](447%20Shamus%20x%201980.jpg)

## Concept UI
![The game was designed around a base color for each mode of the game, it still remains in the final d](420%20The%20game%20was%20designed%20around%20a%20base%20color%20for%20each%20mode%20of%20the%20game,%20it%20still%20remains%20in%20the%20final%20d.jpg)
![Karl the Jarl - scrapped_ David Martel didn't like the idea](423%20Karl%20the%20Jarl%20-%20scrapped_%20David%20Martel%20didn't%20like%20the%20idea.jpg)
![Character Fight Screen - this was scrapped](424.jpg)
![Original Game UI](425.jpg)
![428](428.jpg)
![429](429.jpg)
![430](430.jpg)
![431](431.jpg)
![438](438.jpg)
![439](439.jpg)

## Late stage assets
![Jack - at this point, we decided to make him younger](421%20Jack%20-%20at%20this%20point,%20we%20decided%20to%20make%20him%20younger.jpg)
![Gloria, the intern](418.jpg)
![Karl, described as He-Man in a sweater vest](432%20Karl,%20described%20as%20He-Man%20in%20a%20sweater%20vest.jpg)
![Robert the Father_ He was originally an evil priest, but then was finalized as a lawyer](436%20Robert%20the%20Father_%20He%20was%20originally%20an%20evil%20priest,%20but%20then%20was%20finalized%20as%20a%20lawyer.jpg)
![Robert - his hair and beard remained.jpg](433%20Robert%20-%20his%20hair%20and%20beard%20remained.jpga).jpg)
![Steve, the CEO of The Company_ His design was spot on from the start, so it never changed](437%20Steve,%20the%20CEO%20of%20The%20Company_%20His%20design%20was%20spot%20on%20from%20the%20start,%20so%20it%20never%20changed.jpg)

![!!](448%20!!.jpg)
![Even though our game's engine was written in c++, we did all our animation in Flash and exported it in XML to be converted to our animation format](457%20Even%20though%20our%20game's%20engine%20(by%20Shane)%20is%20in%20C++,%20we%20needed%20a%20quick%20way%20to%20mockup%20all%20the%20animatio.jpg)
![Character Select Screen](458%20Character%20Select%20Screen.jpg)
![Options Backdrop](459%20Options%20Backdrop.jpg)
![Main Menu](460%20Main%20Menu.jpg)
![461](461.jpg)
![462](462.jpg)
![Jack x Caffeine](463%20Jack%20x%20Caffeine.jpg)
![Character Info Screen Base](464%20Character%20Info%20Screen%20Base.jpg)
![Jolie x Caffeine](465%20Jolie%20x%20Caffeine.jpg)
![j_c magnus x caffeine](466%20j_c%20magnus%20x%20caffeine.jpg)
![Shamus x Caffeine](467%20Shamus%20x%20Caffeine.jpg)
![If it weren't for git, I would have killed Shane_ - David](468%20If%20it%20weren't%20for%20git,%20I%20would%20have%20killed%20Shane_%20-%20David.jpg)
![470](470.jpg)
![Karl x Caffeine](471%20Karl%20x%20Caffeine.jpg)
![Robert x Caffeine](474%20Robert%20x%20Caffeine.jpg)

![Character Redesign, the original design was too _sugary_ so we decided to make everything less so](427%20Character%20Redesign,%20the%20original%20design%20was%20too%20_sugary_%20so%20we%20decided%20to%20make%20everything%20less%20so.jpg)