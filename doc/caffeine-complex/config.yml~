book:
    title:            "Developer Notes"
    author:           "2 Davids and a Shane"
    edition:          "David Martel, David Nuon, Shane Satterfield"
    language:         en
    publication_date: ~

    generator: { name: easybook, version: 5.0-DEV }

    contents:
        # available content types: acknowledgement, afterword, appendix, author,
        # chapter, conclusion, cover, dedication, edition, epilogue, foreword,
        # glossary, introduction, license, lof (list of figures), lot (list of
        # tables), part, preface, prologue, title, toc (table of contents)
        - { element: cover }
        - { element: title }
        - { element: toc }
        - { element: license }
        - { element: chapter, number: 1, content: manual.md.compiled }
        - { element: chapter, number: 2, content: software.md.compiled }
        - { element: chapter, number: 3, content: usecase.md }
        - { element: chapter, number: 3, content: usecase.md }

    editions:
        ebook:
            format:         epub
            highlight_code: false
            include_styles: true
            use_html_toc:   false
            labels:         ['appendix', 'chapter']  # labels also available for: "figure", "table"
            theme:          caffeine
            toc:
                deep:       1
                elements:   ["appendix", "chapter", "part"]

        print:
            format:         pdf
            highlight_code: true
            include_styles: true
            isbn:           ~
            labels:         ["appendix", "chapter"]  # labels also available for: "figure", "table"
            margin:
                top:        25mm
                bottom:     25mm
                inner:      30mm
                outter:     20mm
            page_size:      A4
            theme:          caffeine
            toc:
                deep:       6
                elements:   ["appendix", "chapter", "part"]
            two_sided:      true

        web:
            format:         html
            highlight_code: true
            include_styles: true
            labels:         ["appendix", "chapter"]  # labels also available for: "figure", "table"
            theme:          caffeine
            toc:
                deep:       2
                elements:   ["appendix", "chapter"]

        website:
            extends:        web
            format:         html_chunked
                            
